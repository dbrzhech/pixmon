import sqlite3
import smtplib
import os
import socket
import logging
hostname = socket.gethostname()




showphasesthismachine='grep -A 27 ^FED\# /tmp/PixelFED* | grep "Fiber RDY SET  RD    pattern:" -B3 -A24 '
showfaultyphasesthismachine='grep -A 27 ^FED\# /tmp/PixelFED* | grep "Fiber RDY SET  RD    pattern:" -B3 -A24 | grep -e "00000000000000000000000000000000" -e "11111111111111111111111111111111" -e "Fiber" -e FED | grep -B2  -e "00000000000000000000000000000000" -e "11111111111111111111111111111111"'
showphases='grep "FED# " -A24 `ls -1 -tr /pixelraid/pixel/users/ph1commissioning/bpix/*199* | tail -n 8`; ls -1 -tr *199* | tail -n 8'
showfaultyphases='grep -e "FED# " -e 1111111111111111111111 -e "00000000000000000000000000000000" `ls -1 -tr /pixelraid/pixel/users/ph1commissioning/bpix/*199* | tail -n 8`; ls -1 -tr *199* | tail -n 1 '
showfaultyphasesfaultyfed='grep -e "FED# " -e 1111111111111111111111 -e "00000000000000000000000000000000" `ls -1 -tr /pixelraid/pixel/users/ph1commissioning/bpix/*199* | tail -n 8` | grep 1111111111111111 -B1 | cut -b 90-130'
#phases = os.popen(showphases).readlines()
#faultyphases = os.popen(showfaultyphases).readlines()
#faultyphasesfaultyfed = os.popen(showfaultyphasesfaultyfed).readlines()


# Check phases in /tmp
checkphases = 'grep -e "FED# " -e "00000000000000000000000000000000" -e "11111111111111111111111111111111" /tmp/PixelFED* | grep -e 1111111111111111 -e "00000000000000000000000000000000" -B1'
# improved checkphases version to not get SEUs:
checkphases = 'grep -A 27 ^FED\# /tmp/PixelFED* | grep "Fiber RDY SET  RD    pattern:" -B3 -A24 | grep -e "00000000000000000000000000000000" -e "11111111111111111111111111111111" -e "Fiber" -e FED | grep -B2  -e "00000000000000000000000000000000" -e "11111111111111111111111111111111"'
phases = os.popen(checkphases).readlines()
phases_nice = ''.join([line for line in phases])
text = "Phases on " + hostname + ":\n\n" + str(phases_nice)




# Check phases on all machines:
#phases = {}
#hosts = ["srv-s2b18-37-01",
#    "srv-s2b18-34-01",
#    "srv-s2b18-33-01",
#    "srv-s2b18-32-01",
#    "srv-s2b18-41-01",
#    "srv-s2b18-40-01",
#    "srv-s2b18-39-01",
#    "srv-s2b18-38-01",
#    "srv-s2b18-31-01",
#    "srv-s2b18-30-01",
#    "srv-s2b18-29-01",
#    "srv-s2b18-28-01"]

#for host in hosts:
#    checkphases = 'ssh ' + host + ' grep "00000000000000000000000000000000" /tmp/PixelFED*;grep "11111111111111111111111111111111" /tmp/PixelFED*'
#    print checkphases
#    phases[host] = os.popen(checkphases).readlines()
#
#for host in phases:
#    text += "Phases in " + host + ":\n\n" + phases[host]
#    text += "="*50








runfilename = "/nfshome0/jsonneve/logs/phases/testphases.log"
format='%(asctime)s %(levelname)s %(message)s'
logging.basicConfig(filename=runfilename, level=0, format=format)
logging.info(text)

if phases != []:
    afzender='jory.sonneveld@cern.ch'
    recipient = 'jory.sonneveld@desy.de'
    smtpconn = smtplib.SMTP(host='localhost',port=25)
    #text="Phases:\n\n" + showphases + "\n\n" + phases
    #text += "="*50
    #text += "Faulty phases:\n\n" + showfaultyphases + "\n\n" + faultyphases
    #text += "="*50
    #text += "Faulty phases only if error:\n\n" + showfaultyphasesfaultyfed + "\n\n" + faultyphasesfaultyfed
    #text += "="*50
    subject= "phases on " + hostname
    message = 'Subject: {}\n\n{}'.format(subject, text)
    print(message)
    smtpconn.sendmail(afzender, recipient, message)
    smtpconn.close()



