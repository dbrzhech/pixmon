with maxsince as (select max(since) latest from cms_trk_dcs_pvss_cond.aliases where
    (alias like '%PixelBarrel_BmI_%L4%'--'%PixelBarrel_BmI_1I_L4D2MN%'
    )
    --and dpe_name like '%/channel001.'
    group by alias),
    dpname as (select rtrim(dpe_name,'.') dp, alias from cms_trk_dcs_pvss_cond.aliases,maxsince
    where
    (alias like '%PixelBarrel_BmI_%L4%'--'%PixelBarrel_BmI_1I_L4D2MN%'
    )
    --and dpe_name like '%/channel001.'
    and since = maxsince.latest),
ident as (select id from cms_trk_dcs_pvss_cond.dp_name2id, dpname
    where cms_trk_dcs_pvss_cond.dp_name2id.dpname = dpname.DP)
select dpname.alias,rb.dpe_name, rb.ts, rb.value_number from cms_trk_dcs_pvss_cond.eventhistory_rb rb,ident,dpname
where rb.dpid = ident.id -- and dpe_name = 'ACTUAL_VMON' -- 'ACTUAL_IMON' --alternatively -- only temperature at the moment
and value_number is not null
and rb.ts > to_timestamp('2018-07-20 20:40:00.0', 'RRRR-MM-DD HH24:MI:SS.FF')
and rb.ts < to_timestamp('2018-07-23 14:30:00.0', 'RRRR-MM-DD HH24:MI:SS.FF') --and rownum < 10
order by dpname.alias,rb.ts;
