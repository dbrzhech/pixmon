---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER/PixelBarrel/PixelBarrel_BmI/Configured
VALUE_CONVERTED
21-JUL-18 07.02.08.834001000 PM
      50


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER/PixelBarrel/PixelBarrel_BmI/Configured
VALUE_CONVERTED
21-JUL-18 07.02.18.817000000 PM
      50


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER/PixelBarrel/PixelBarrel_BmI/Configured
VALUE_CONVERTED
21-JUL-18 07.02.18.817000000 PM
      50


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER/PixelBarrel/PixelBarrel_BmI/Configured
VALUE_CONVERTED
21-JUL-18 07.02.18.817001000 PM
      50


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
CMS_TRACKER/PixelBarrel/PixelBarrel_BmI/Configured

39840 rows selected.


with maxsince as (select max(since) latest from cms_trk_dcs_pvss_cond.aliases where
  2      (alias like '%PixelBarrel_BmI_%L4%'--'%PixelBarrel_BmI_1I_L4D2MN%'
    )
    --and dpe_name like '%/channel001.'
  5      group by alias),
    dpname as (select rtrim(dpe_name,'.') dp, alias from cms_trk_dcs_pvss_cond.aliases,maxsince
    where
  8      (alias like '%PixelBarrel_BmI_%L4%'--'%PixelBarrel_BmI_1I_L4D2MN%'
    )
    --and dpe_name like '%/channel001.'
 11      and since = maxsince.latest),
ident as (select id from cms_trk_dcs_pvss_cond.dp_name2id, dpname
 13      where cms_trk_dcs_pvss_cond.dp_name2id.dpname = dpname.DP)
select dpname.alias,rb.dpe_name, rb.ts, rb.value_number from cms_trk_dcs_pvss_cond.eventhistory_rb rb,ident,dpname
 15  where rb.dpid = ident.id -- and dpe_name = 'ACTUAL_VMON' -- 'ACTUAL_IMON' --alternatively -- only temperature at the moment
and value_number is not null
 17  and rb.ts > to_timestamp('2018-07-20 20:40:00.0', 'RRRR-MM-DD HH24:MI:SS.FF')
and rb.ts < to_timestamp('2018-07-23 14:30:00.0', 'RRRR-MM-DD HH24:MI:SS.FF') --and rownum < 10
 19  order by dpname.alias,rb.ts;

ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1I_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1M_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_1R_L4D2MN/write
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6I_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6M_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/readback
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.27.16.318000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.27.33.836000000 PM
    -.36


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.29.11.332000000 PM
    -.49


ALIAS
--------------------------------------------------------------------------------
DPE_NAME
------------------------------
TS
---------------------------------------------------------------------------
VALUE_NUMBER
------------
CMS_TRACKER_PLCS/Pixel/PixelBarrel_BmI/PixelBarrel_BmI_6R_L4D4MN/write
VALUE_CONVERTED
23-JUL-18 02.29.28.834000000 PM
       -1.65


