import sys,time,os, re
from datetime import date
from time import sleep
#from ROOT import TH1F, TH2F, TFile, gStyle, gPad, TCanvas,gDirectory

infile="moduleCurrents.txt"

#loopname=sys.argv[1]
loopnames =[
"L1D1PN",
"L1D2MN", 
"L2D1MN", 
"L2D2PN",
"L3D1MN",
"L3D2PN",
"L3D3MN",
"L3D4PN",
"L4D1PN",
"L4D2MN",
"L4D3PN",
"L4D4MN",
"L1D1MF",
"L1D2PF",
"L2D1PF",
"L2D2MF",
"L3D1PF",
"L3D2MF",
"L3D3PF",
"L3D4MF",
"L4D1MF",
"L4D2PF",
"L4D3MF",
"L4D4PF"]

quad=[]
sec=[]

for loopname in loopnames:

    if ("N" in loopname):
        quad.append("BpI")
        quad.append("BmI")
    
    if ("F" in loopname):
        quad.append("BpO")
        quad.append("BmO")

    if ("L1D1" in loopname):
        sec.append("SEC1_LYR1")
        sec.append("SEC2_LYR1")
        sec.append("SEC3_LYR1")
        sec.append("SEC4_LYR1")

    if ("L1D2" in loopname):
        sec.append("SEC5_LYR1")
        sec.append("SEC6_LYR1")
        sec.append("SEC7_LYR1")
        sec.append("SEC8_LYR1")

    if ("L2D1" in loopname):
        sec.append("SEC1_LYR2")
        sec.append("SEC2_LYR2")
        sec.append("SEC3_LYR2")
        sec.append("SEC4_LYR2")

    if ("L2D2" in loopname):
        sec.append("SEC5_LYR2")
        sec.append("SEC6_LYR2")
        sec.append("SEC7_LYR2")
        sec.append("SEC8_LYR2")

    if ("L3D1" in loopname):
        sec.append("SEC1_LYR3")
        sec.append("SEC2_LYR3")
    
    if ("L3D2" in loopname):
        sec.append("SEC3_LYR3")
        sec.append("SEC4_LYR3")

    if ("L3D3" in loopname):
        sec.append("SEC5_LYR3")
        sec.append("SEC6_LYR3")
    
    if ("L3D4" in loopname):
        sec.append("SEC7_LYR3")
        sec.append("SEC8_LYR3")

    if ("L4D1" in loopname):
        sec.append("SEC1_LYR4")
        sec.append("SEC2_LYR4")

    if ("L4D2" in loopname):
        sec.append("SEC3_LYR4")
        sec.append("SEC4_LYR4")

    if ("L4D3" in loopname):
        sec.append("SEC5_LYR4")
        sec.append("SEC6_LYR4")

    if ("L4D4" in loopname):
        sec.append("SEC7_LYR4")
        sec.append("SEC8_LYR4")
    
    

    meani=0
    ni=0
    for l in open(infile).readlines():
        #print l
        for q in quad:
            #print q
            if (q in l):
                #print l
                for s in sec:
                    #print s
                    if s in l:
                        #print l
                        i = float(l.split(" ")[1])
                        if (i>0):
                            #print l
                            meani=meani+i
                            ni=ni+1

    print "Mean leakge current for loop ", loopname, ": ", meani/ni, "uA"
    #print meani/ni
  
