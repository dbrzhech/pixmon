import os
from datetime import datetime
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib.pyplot import Line2D
import sql3 as sql
import sys


if __name__ == "__main__":
    database = "temps.db"
    table = "currents"
    if len(sys.argv) > 1:
        starttime = sys.argv[1]
        #stoptime = datetime.datetime(starttime make starttime + 3h?
    if len(sys.argv) > 2:
        stoptime = sys.argv[2]
    else:
        print "usage e.g."
        starttime = "2017-10-05 13:00:45.421363"
        stoptime = "2017-10-06 05:00:45.421363"
        print "python getTemps.py", "'" + starttime + "' '" + stoptime + "'"
        sys.exit()
    temperatures = sql.dicts_from_db_table(database, table, constraints = {}, gt = {'change_date': '"' + starttime + '"'}, lt = {'change_date': '"' + stoptime + '"'})
    bpix_air = [d for d in temperatures if 'barrel' in d['name'].lower() and 'TSil' in d['probe_type'] and 'Detector' in d['name']]


    fig = plt.figure(figsize=(15, 8))
    ax = fig.add_subplot(111)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
    colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
    markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
    markers.extend(list(Line2D.filled_markers))
    order = range(len(colors))
    #ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
    colorno = 0
    markerno = 0
    probenames_bpixair = list(set([d['name'] for d in bpix_air]))



    annotations = {

            #'BPix BmO5 L34 drop out': mdates.date2num(datetime.strptime("2017-10-05 21:29", '%Y-%m-%d %H:%M')),
            }
    for probename in probenames_bpixair:
        colorno = colorno % len(colors)
        markerno = markerno % len(markers)
        tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
        dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
        tmps = [float(temp) for temp in tmpsraw]
        ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
        colorno += 1
        markerno += 1

    plt.legend(loc='lower right', ncol=2)
    fig.autofmt_xdate(rotation=45)
    #fig.tight_layout()
    plt.ylabel('$^\circ$C', fontsize=18)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    #plt.xlim((min(dates), max(dates)))
    #plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
    ymin = -15
    ymax = 30
    for run in annotations:
        plt.vlines(annotations[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
        if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
            plt.annotate(run, (annotations[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
        elif 'tracker' in run.lower():
            plt.annotate(run, (annotations[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
        else:
            plt.annotate(run, (annotations[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)
        notfirst = True
    plt.ylim((ymin, ymax))
    plt.title("BPix temperatures", fontsize=18)
    handles, labels = ax.get_legend_handles_labels()
    if len(labels) > 20:
        lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5)) #, fontsize=14)
    elif len(labels) > 40:
        lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5)) #, fontsize=14)
    else:
        lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5)) #, fontsize=14)
    # Make grid lines on plot:
    ax.grid(True)
    plt.savefig("bpix_temperatures_test.png", bbox_inches='tight')


