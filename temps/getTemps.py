import cx_Oracle
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
###from numberOfROCs import numberOfRocs

if __name__ == "__main__":
            ### Opening connection
            connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_adg')
            # connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_lb')
            cursor = connection.cursor()
            cursor.arraysize=50

    ### Define query to get currents for Barrel Pixels


            query2="""
with IDs as ( select id, alias, rtrim(dpe_name,'.') as dp, substr(alias,instr(alias,'/',-1)+1) as part, CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname as dpname from CMS_TRK_DCS_PVSS_COND.aliases, CMS_TRK_DCS_PVSS_COND.dp_name2id where upper(alias) like '%PIXEL%' and since > to_timestamp('2016-10-01', 'RRRR-MM-DD') and rtrim(CMS_TRK_DCS_PVSS_COND.aliases.dpe_name,'.') = CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname)
select part, alias, value_converted, change_date , dpname from CMS_TRK_DCS_PVSS_COND.tkplcreadsensor, IDs where IDs.id = CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.DPID and CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date >= to_timestamp('2017-09-18 14:35:45.421363', 'RRRR-MM-DD HH24:MI:SS.FF') AND CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date < to_timestamp('2017-09-30 22:05:45.421363', 'RRRR-MM-DD HH24:MI:SS.FF') order by part, CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date
"""
            query2="""
with IDs as ( select id, alias, since, rtrim(dpe_name,'.') as dp, substr(alias,instr(alias,'/',-1)+1) as part, update_count, CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname as dpname from CMS_TRK_DCS_PVSS_COND.aliases, CMS_TRK_DCS_PVSS_COND.dp_name2id where upper(CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname) like  '%PIXEL%' and  rtrim(CMS_TRK_DCS_PVSS_COND.aliases.dpe_name,'.') = CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname)
select part, alias, since, update_count, value_converted, change_date , dpname from CMS_TRK_DCS_PVSS_COND.tkplcreadsensor, IDs where IDs.id = CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.DPID and CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date >= to_timestamp('2017-09-18 15:00:45.421363', 'RRRR-MM-DD HH24:MI:SS.FF') AND CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date < to_timestamp('2017-09-30 15:00:45.421363', 'RRRR-MM-DD HH24:MI:SS.FF') order by part, CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date
"""

            print query2
            cursor.execute(query2)
            #{"the_start_time" : begintime, "the_end_time" : endtime})
            row = cursor.fetchall()
#
            fileCurrents = "temps.txt"
            fcur = open(fileCurrents, "w+")

            for i in xrange(len(row)):
                #print len(row)
                print "====> ", "".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n"
                fcur.write("".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n")
                #fcur.write(str(row[i][0]) + "   " + str(row[i][1]) + "   " + str(row[i][2])+ "\n")

            fcur.close()


            connection.close()






