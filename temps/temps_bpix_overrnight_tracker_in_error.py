
# coding: utf-8

# In[302]:


get_ipython().magic(u'pylab inline')
import os
from datetime import datetime
import matplotlib.dates as mdates


# In[303]:

os.listdir('.')


# In[304]:

temps = open('pix_all_temps_night_tracker_in_error.dat')


# In[305]:

line = temps.readline()


# In[306]:

line


# In[307]:

keys = line.split('|')


# In[308]:

temperatures = []
line = temps.readline()
lilne = temps.readline()


# In[309]:

while line != '':
    temperatures.append({keys[k].strip(): line.split('|')[k].strip() for k in range(len(keys))})
    line = temps.readline()


# In[310]:

temperatures[3]


# In[316]:

bpix_air = [d for d in temperatures if 'barrel' in d['name'].lower() and 'TSil' in d['probe_type'] and 'Detector' in d['name']]


# In[317]:

bpix_air[0]


# In[318]:

probenames = list(set([d['name'] for d in temperatures]))
probenames_bpixair = list(set([d['name'] for d in bpix_air]))

probenames_bpixair


# In[319]:

bpi2 = [d for d  in bpix_air if 'BpO Sector 2' in d['name']]
for d in bpi2:
    if d['change_date'] > '2017-09-21 09:00':
        print d['datapoint_name'], d['change_date'], d['value_converted']


# In[320]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(markers)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
    colorno += 1
    markerno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1654 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1654 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 20:47"
runs['SS2 problem -- tracker into error'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:10"
runs['Tracker back on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 08:59"
runs['Run 1656 SCurve start (with HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:45"
runs['Run 1657 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))

# After also until 01:40 (HV off) --> do Run 1653,4 again.

plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 30


for run in runs:
    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
    if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
        plt.annotate(run, (runs[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
    elif 'tracker' in run.lower():
        plt.annotate(run, (runs[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
    else:
        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)

    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error.png", bbox_inches='tight')




# In[333]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(markers)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
    colorno += 1
    markerno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1654 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1654 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 20:47"
runs['SS2 problem -- tracker into error'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:10"
runs['Tracker back on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 08:59"
runs['Run 1656 SCurve start (with HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:45"
runs['Run 1657 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))

# After also until 01:40 (HV off) --> do Run 1653,4 again.

plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 30


for run in runs:
    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
    if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
        plt.annotate(run, (runs[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
    elif 'tracker' in run.lower():
        plt.annotate(run, (runs[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
    else:
        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)

    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)

xminus = "2017-09-21 05:47"
xplus = max(dates)
plt.xlim((mdates.date2num(datetime.strptime(xminus, '%Y-%m-%d %H:%M')), xplus))
plt.ylim((5, 30))

plt.savefig("bpix_temperatures_overnight_tracker_error_zoom.png", bbox_inches='tight')




# In[321]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(markers)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    if 'BpI' in probename or 'BpO' in probename:
        ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
    colorno += 1
    markerno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1654 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1654 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 20:47"
runs['SS2 problem -- tracker into error'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:10"
runs['Tracker back on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 08:59"
runs['Run 1656 SCurve start (with HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:45"
runs['Run 1657 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))

# After also until 01:40 (HV off) --> do Run 1653,4 again.

plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 30


for run in runs:
    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
    if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
        plt.annotate(run, (runs[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
    elif 'tracker' in run.lower():
        plt.annotate(run, (runs[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
    else:
        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)

    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error_plus.png", bbox_inches='tight')




# In[322]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(markers)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    if 'BmI' in probename or 'BmO' in probename:
        ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
    colorno += 1
    markerno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1654 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1654 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 20:47"
runs['SS2 problem -- tracker into error'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:10"
runs['Tracker back on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 08:59"
runs['Run 1656 SCurve start (with HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:45"
runs['Run 1657 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))

# After also until 01:40 (HV off) --> do Run 1653,4 again.

plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 30


for run in runs:
    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
    if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
        plt.annotate(run, (runs[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
    elif 'tracker' in run.lower():
        plt.annotate(run, (runs[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
    else:
        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)

    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error_minus.png", bbox_inches='tight')




# In[323]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(markers)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    if 'BmI' in probename or 'BpI' in probename:
        ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
    colorno += 1
    markerno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1654 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1654 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 20:47"
runs['SS2 problem -- tracker into error'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:10"
runs['Tracker back on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 08:59"
runs['Run 1656 SCurve start (with HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:45"
runs['Run 1657 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))

# After also until 01:40 (HV off) --> do Run 1653,4 again.

plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 30


for run in runs:
    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
    if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
        plt.annotate(run, (runs[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
    elif 'tracker' in run.lower():
        plt.annotate(run, (runs[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
    else:
        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)

    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error_inner.png", bbox_inches='tight')




# In[324]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(markers)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    if 'BmO' in probename or 'BpO' in probename:
        ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
    colorno += 1
    markerno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1654 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1654 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 20:47"
runs['SS2 problem -- tracker into error'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:10"
runs['Tracker back on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 08:59"
runs['Run 1656 SCurve start (with HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:45"
runs['Run 1657 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))

# After also until 01:40 (HV off) --> do Run 1653,4 again.

plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 30


for run in runs:
    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
    if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
        plt.annotate(run, (runs[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
    elif 'tracker' in run.lower():
        plt.annotate(run, (runs[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
    else:
        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)

    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error_outer.png", bbox_inches='tight')




# In[325]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(markers)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    if 'BpO Sector 1' in probename or 'BpO Sector 2' in probename or 'BpO Sector 3' in probename or 'BpO Sector 4' in probename:
        ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[markerno], s=80)
    colorno += 1
    markerno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1654 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1654 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 20:47"
runs['SS2 problem -- tracker into error'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:10"
runs['Tracker back on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 08:59"
runs['Run 1656 SCurve start (with HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 10:45"
runs['Run 1657 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))

# After also until 01:40 (HV off) --> do Run 1653,4 again.

plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 30


for run in runs:
    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
    if (not 'power' in run.lower() and not '1646' in run.lower() and not 'SS2' in run): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
        plt.annotate(run, (runs[run], 10), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
    elif 'tracker' in run.lower():
        plt.annotate(run, (runs[run], 26), rotation=90, xytext=(-15, 10), textcoords='offset points', fontsize=16)
    else:
        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(-25, 5), textcoords='offset points', fontsize=16)

    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error_bpo1234.png", bbox_inches='tight')




# In[326]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0
markerno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    markerno = markerno % len(mareke)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[colorno], s=80)
    colorno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1656 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1656 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# After also until 01:40 (HV off) --> do Run 1653,4 again.
plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 25



plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error_nolabels.png", bbox_inches='tight')




# In[238]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0


for probename in probenames_bpixair:
    colorno = colorno % len(colors)
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    if 'BmO' in probename and '4' in probename:
        ax.scatter(dates, tmps, label=probename, color = colors[colorno], marker=markers[colorno], s=80)
    colorno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1656 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1656 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 16:39"
runs['BPix powered off'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# After also until 01:40 (HV off) --> do Run 1653,4 again.
plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 25



plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)

handles, labels = ax.get_legend_handles_labels()
if len(labels) > 20:
    lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
elif len(labels) > 40:
    lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
else:
    lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

# Make grid lines on plot:
ax.grid(True)




plt.savefig("bpix_temperatures_overnight_tracker_error.png", bbox_inches='tight')




# In[211]:


fig = plt.figure(figsize=(15, 8))
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
markers = [r'$\bowtie$', r'$\clubsuit$', r'$\circlearrowleft$', r'$\checkmark$', '8', 's', '*', 'D', 'p']
markers.extend(list(Line2D.filled_markers))

order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
colorno = 0


for probename in probenames_bpixair:
    tmpsraw, datesraw = zip(*[(d['value_converted'], d['change_date']) for d in bpix_air if d['name'] == probename])
    dates = [mdates.date2num(datetime.strptime(dat, '%Y-%m-%d %H:%M:%S.%f')) for dat in datesraw]
    tmps = [float(temp) for temp in tmpsraw]
    ax.scatter(dates, tmps, label=probename[:probename.index('T')], color = colors[colorno], marker=markers[colorno], s=80)
    colorno += 1
runs = {}
# pixelalive run 1645 23:00 - 23:36 on sep 20:
run1645finish = "2017-09-20 23:36"
runs['Run 1645 PixelAlive'] = mdates.date2num(datetime.strptime(run1645finish, '%Y-%m-%d %H:%M'))
#run1645start = "2017-09-20 23:00"
#pixelalive1645start = mdates.date2num(datetime.strptime(run1645start, '%Y-%m-%d %H:%M'))
#plt.fill_betweenx(0, pixelalive1645start, pixelalive1645finish)
# ianabpixfast 23:48 run 1646 sep 20

#Run 1646 ianaBPixFast 23:39
finish = "2017-09-20 23:48"
runs['Run 1646 IanaBPIXFast'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1647 VallBPIX
run1647finish = "2017-09-21 0:02"
runs['Run 1647 VallBPIX'] = mdates.date2num(datetime.strptime(run1647finish, '%Y-%m-%d %H:%M'))
# trim 2 --> trim 4
# Run 1648 PixelNoiseScan
finish = "2017-09-21 0:26"
runs['Run 1648 PixelNoiseScan'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1649 IanaBPixFast 0:28
# Run 1650 VallBPIX 0:44
finish = "2017-09-21 0:44"
runs['Run 1650 VallBPIX'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# Run 1651 SCurveVtrim +10
# Run 1652 SCurveVtrim -10 1:46 crashed (see below)
# Run 1653 SCurveVtrim -10 2:06
# Run 1654 SCurve 02:44 (running)
finish = "2017-09-21 2:44"
runs['Run 1656 SCurve start (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-21 4:13"
runs['Run 1656 SCurve finish (no HV)'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
finish = "2017-09-20 22:54"
runs['BPix powered on'] = mdates.date2num(datetime.strptime(finish, '%Y-%m-%d %H:%M'))
# After also until 01:40 (HV off) --> do Run 1653,4 again.
plt.legend(fontsize=20, loc='lower right', ncol=2)
fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlim((min(dates), max(dates)))
#plt.vlines(pixelalive1645finish, plt.ylim()[0], plt.ylim()[1])#, label='Run 1645 PixelAlive')
ymin = -15
ymax = 10


#for run in runs:
#    plt.vlines(runs[run], ymin, ymax, linestyle='--', color='orange', linewidth=3)#, label='Run 1645 PixelAlive')
#    if not 'power' in run.lower() and not '1646' in run.lower(): #'scurve' in run.lower() or 'pixelnoise' in run.lower():
#        plt.annotate(run, (runs[run], 5), rotation=90, xytext=(3, 0), textcoords='offset points', fontsize=16)
#    else:
#        plt.annotate(run, (runs[run], 2), rotation=90, xytext=(-25, 0), textcoords='offset points', fontsize=16)

#    notfirst = True
#plt.annotate( (pixelalive1645finish, 0), 'Run 1645 PixelAlive')
plt.ylim((ymin, ymax))
plt.title("BPix temperatures", fontsize=18)
plt.savefig("bpix_temperatures_overnight_tracker_error_noruns.png", bbox_inches='tight')


# In[170]:

help(plt.annotate)


# In[ ]:



