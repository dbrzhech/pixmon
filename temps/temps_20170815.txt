name                                                probe_type  fsm_part              value_conv  change_date                     datapoint_name                                              
--------------------------------------------------  ----------  --------------------  ----------  ------------------------------  ------------------------------------------------------------
PixelEndcap BpI DCDC 3A                             TAir        PixelEndCap_BpI       -7.33       2017-08-15 04:00:25.666000      TK_PLCS/Pixel/crate02/module04/channel_read_01              
PixelEndcap BpI DCDC 3B                             TAir        PixelEndCap_BpI       -10.58      2017-08-15 03:56:25.670000      TK_PLCS/Pixel/crate02/module04/channel_read_02              
PixelEndcap BpI Disk 3O                             TSil        PixelEndCap_BpI       -16.97      2017-08-15 03:54:48.263000      TK_PLCS/Pixel/crate02/module04/channel_read_03              
PixelEndcap BpI Disk 2O                             TSil        PixelEndCap_BpI       -16.41      2017-08-14 06:27:00.663000      TK_PLCS/Pixel/crate02/module04/channel_read_05              
PixelEndcap BpI DCDC 3C                             TAir        PixelEndCap_BpI       -10.1       2017-08-15 03:56:05.663000      TK_PLCS/Pixel/crate02/module05/channel_read_01              
PixelEndcap BpI DCDC 3D                             TAir        PixelEndCap_BpI       -7.57       2017-08-15 03:55:50.668000      TK_PLCS/Pixel/crate02/module05/channel_read_02              
PixelEndcap BpO DCDC 3A                             TAir        PixelEndCap_BpO       -9.41       2017-08-15 03:54:33.162000      TK_PLCS/Pixel/crate02/module06/channel_read_01              
PixelEndcap BpO DCDC 3B                             TAir        PixelEndCap_BpO       -8.68       2017-08-15 03:52:55.656000      TK_PLCS/Pixel/crate02/module06/channel_read_02              
PixelEndcap BpO Disk 3O                             TSil        PixelEndCap_BpO       -16.26      2017-08-15 03:54:50.661000      TK_PLCS/Pixel/crate02/module06/channel_read_03              
PixelEndcap BpO Disk 3I                             TSil        PixelEndCap_BpO       -16.18      2017-08-14 06:27:20.671000      TK_PLCS/Pixel/crate02/module06/channel_read_04              
PixelEndcap BpO Disk 2O                             TSil        PixelEndCap_BpO       -16.41      2017-08-15 03:57:45.669000      TK_PLCS/Pixel/crate02/module06/channel_read_05              
PixelEndcap BpO Disk 2I                             TSil        PixelEndCap_BpO       -16.94      2017-08-15 03:56:18.273000      TK_PLCS/Pixel/crate02/module06/channel_read_06              
PixelEndcap BpO DCDC 3C                             TAir        PixelEndCap_BpO       -8.16       2017-08-15 03:43:25.667000      TK_PLCS/Pixel/crate02/module07/channel_read_01              
PixelEndcap BpO DCDC 3D                             TAir        PixelEndCap_BpO       -10.83      2017-08-15 03:54:33.162000      TK_PLCS/Pixel/crate02/module07/channel_read_02              
PixelEndcap BmI DCDC 3A                             TAir        PixelEndCap_BmI       -10.34      2017-08-15 03:54:50.661000      TK_PLCS/Pixel/crate02/module08/channel_read_01              
PixelEndcap BmI DCDC 3B                             TAir        PixelEndCap_BmI       -7.64       2017-08-15 03:53:35.678000      TK_PLCS/Pixel/crate02/module08/channel_read_02              
PixelEndcap BmI Disk 3I                             TSil        PixelEndCap_BmI       -16.26      2017-08-15 03:55:58.170000      TK_PLCS/Pixel/crate02/module08/channel_read_04              
PixelEndcap BmI Disk 2I                             TSil        PixelEndCap_BmI       -15.35      2017-08-15 03:52:48.255000      TK_PLCS/Pixel/crate02/module08/channel_read_06              
PixelEndcap BmI DCDC 3C                             TAir        PixelEndCap_BmI       -8.18       2017-08-15 03:42:53.168000      TK_PLCS/Pixel/crate02/module09/channel_read_01              
PixelEndcap BmI DCDC 3D                             TAir        PixelEndCap_BmI       -10.41      2017-08-15 03:52:23.172000      TK_PLCS/Pixel/crate02/module09/channel_read_02              
PixelEndcap BmO DCDC 3A                             TAir        PixelEndCap_BmO       -8.03       2017-08-15 03:55:20.664000      TK_PLCS/Pixel/crate02/module10/channel_read_01              
PixelEndcap BmO DCDC 3B                             TAir        PixelEndCap_BmO       -10.53      2017-08-15 03:54:30.664000      TK_PLCS/Pixel/crate02/module10/channel_read_02              
PixelEndcap BmO Disk 3O                             TSil        PixelEndCap_BmO       -15.03      2017-08-14 06:27:33.168000      TK_PLCS/Pixel/crate02/module10/channel_read_03              
PixelEndcap BmO Disk 3I                             TSil        PixelEndCap_BmO       -16.51      2017-08-15 03:58:18.272000      TK_PLCS/Pixel/crate02/module10/channel_read_04              
PixelEndcap BmO Disk 1O                             TSil        PixelEndCap_BmO       -15.85      2017-08-14 07:13:08.174000      TK_PLCS/Pixel/crate02/module10/channel_read_07              
PixelEndcap BmO DCDC 3C                             TAir        PixelEndCap_BmO       -7.47       2017-08-15 03:53:43.170000      TK_PLCS/Pixel/crate02/module11/channel_read_01              
PixelEndcap BmO DCDC 3D                             TAir        PixelEndCap_BmO       -9.9        2017-08-15 03:55:50.668000      TK_PLCS/Pixel/crate02/module11/channel_read_02              
PixelEndcap BpI PC 1A                               TAir        PixelEndCap_BpI       -11.111111  2017-08-15 03:52:38.164000      TK_PLCS/Pixel/crate03/module04/channel_read_01              
PixelEndcap BpI PC 1B                               TAir        PixelEndCap_BpI       -10.286458  2017-08-15 03:54:00.670000      TK_PLCS/Pixel/crate03/module04/channel_read_02              
PixelEndcap BpI PC 1C                               TAir        PixelEndCap_BpI       -11.197916  2017-08-15 03:51:45.667000      TK_PLCS/Pixel/crate03/module04/channel_read_03              
PixelEndcap BpI PC 1D                               TAir        PixelEndCap_BpI       -10.416666  2017-08-15 03:51:50.668000      TK_PLCS/Pixel/crate03/module04/channel_read_04              
PixelEndcap BpI PC 2A                               TAir        PixelEndCap_BpI       -10.329861  2017-08-15 03:52:05.675000      TK_PLCS/Pixel/crate03/module04/channel_read_05              
PixelEndcap BpI PC 2B                               TAir        PixelEndCap_BpI       -10.894097  2017-08-15 03:53:03.168000      TK_PLCS/Pixel/crate03/module04/channel_read_06              
PixelEndcap BpI PC 2C                               TAir        PixelEndCap_BpI       -11.979166  2017-08-15 03:52:15.674000      TK_PLCS/Pixel/crate03/module04/channel_read_07              
PixelEndcap BpI PC 3A                               TAir        PixelEndCap_BpI       -10.633680  2017-08-15 03:57:58.168000      TK_PLCS/Pixel/crate03/module05/channel_read_01              
PixelEndcap BpI PC 3B                               TAir        PixelEndCap_BpI       -10.850694  2017-08-15 03:51:49.170000      TK_PLCS/Pixel/crate03/module05/channel_read_02              
PixelEndcap BpI PC 3C                               TAir        PixelEndCap_BpI       -11.328125  2017-08-15 03:52:33.168000      TK_PLCS/Pixel/crate03/module05/channel_read_03              
PixelEndcap BpI PC 3D                               TAir        PixelEndCap_BpI       -10.329861  2017-08-15 03:56:55.660000      TK_PLCS/Pixel/crate03/module05/channel_read_04              
PixelEndcap BpI Air                                 TAir        PixelEndCap_BpI       -9.0277777  2017-08-15 03:54:03.168000      TK_PLCS/Pixel/crate03/module05/channel_read_05              
PixelEndcap BpO PC 1A                               TAir        PixelEndCap_BpO       -10.720486  2017-08-15 03:54:15.662000      TK_PLCS/Pixel/crate03/module06/channel_read_01              
PixelEndcap BpO PC 1B                               TAir        PixelEndCap_BpO       -10.9375    2017-08-15 03:51:53.166000      TK_PLCS/Pixel/crate03/module06/channel_read_02              
PixelEndcap BpO PC 1C                               TAir        PixelEndCap_BpO       -10.112847  2017-08-15 03:51:50.668000      TK_PLCS/Pixel/crate03/module06/channel_read_03              
PixelEndcap BpO PC 1D                               TAir        PixelEndCap_BpO       -11.328125  2017-08-15 03:51:45.667000      TK_PLCS/Pixel/crate03/module06/channel_read_04              
PixelEndcap BpO PC 2A                               TAir        PixelEndCap_BpO       -11.284722  2017-08-15 03:55:45.665000      TK_PLCS/Pixel/crate03/module06/channel_read_05              
PixelEndcap BpO PC 2B                               TAir        PixelEndCap_BpO       -11.71875   2017-08-15 03:55:38.157000      TK_PLCS/Pixel/crate03/module06/channel_read_06              
PixelEndcap BpO PC 2C                               TAir        PixelEndCap_BpO       -10.720486  2017-08-15 03:52:00.672000      TK_PLCS/Pixel/crate03/module06/channel_read_07              
PixelEndcap BpO PC 3A                               TAir        PixelEndCap_BpO       -9.1145833  2017-08-15 03:51:55.663000      TK_PLCS/Pixel/crate03/module07/channel_read_01              
PixelEndcap BpO PC 3B                               TAir        PixelEndCap_BpO       -9.0277777  2017-08-15 03:52:08.173000      TK_PLCS/Pixel/crate03/module07/channel_read_02              
PixelEndcap BpO PC 3C                               TAir        PixelEndCap_BpO       -10.763888  2017-08-15 03:51:50.668000      TK_PLCS/Pixel/crate03/module07/channel_read_03              
PixelEndcap BpO PC 3D                               TAir        PixelEndCap_BpO       -11.501736  2017-08-15 03:52:13.170000      TK_PLCS/Pixel/crate03/module07/channel_read_04              
PixelEndcap BpO Air                                 TAir        PixelEndCap_BpO       -4.6006944  2017-08-15 03:55:38.157000      TK_PLCS/Pixel/crate03/module07/channel_read_05              
PixelEndcap BmI PC 1A                               TAir        PixelEndCap_BmI       -10.980902  2017-08-15 03:55:10.661000      TK_PLCS/Pixel/crate03/module08/channel_read_01              
PixelEndcap BmI PC 1B                               TAir        PixelEndCap_BmI       -10.416666  2017-08-15 03:51:55.663000      TK_PLCS/Pixel/crate03/module08/channel_read_02              
PixelEndcap BmI PC 1C                               TAir        PixelEndCap_BmI       -11.067708  2017-08-15 03:51:55.663000      TK_PLCS/Pixel/crate03/module08/channel_read_03              
PixelEndcap BmI PC 1D                               TAir        PixelEndCap_BmI       -9.8524305  2017-08-15 03:51:58.174000      TK_PLCS/Pixel/crate03/module08/channel_read_04              
PixelEndcap BmI PC 2A                               TAir        PixelEndCap_BmI       -9.6354166  2017-08-15 03:51:55.663000      TK_PLCS/Pixel/crate03/module08/channel_read_05              
PixelEndcap BmI PC 2B                               TAir        PixelEndCap_BmI       -9.6788194  2017-08-15 03:51:55.663000      TK_PLCS/Pixel/crate03/module08/channel_read_06              
PixelEndcap BmI PC 2C                               TAir        PixelEndCap_BmI       -10.633680  2017-08-15 03:52:18.283000      TK_PLCS/Pixel/crate03/module08/channel_read_07              
PixelEndcap BmI PC 3A                               TAir        PixelEndCap_BmI       -9.765625   2017-08-15 03:55:45.665000      TK_PLCS/Pixel/crate03/module09/channel_read_01              
PixelEndcap BmI PC 3B                               TAir        PixelEndCap_BmI       -10.199652  2017-08-15 03:55:18.260000      TK_PLCS/Pixel/crate03/module09/channel_read_02              
PixelEndcap BmI PC 3C                               TAir        PixelEndCap_BmI       -8.7673611  2017-08-15 03:58:40.672000      TK_PLCS/Pixel/crate03/module09/channel_read_03              
PixelEndcap BmI PC 3D                               TAir        PixelEndCap_BmI       -10.763888  2017-08-15 03:52:00.672000      TK_PLCS/Pixel/crate03/module09/channel_read_04              
PixelEndcap BmI Air                                 TAir        PixelEndCap_BmI       -5.9895833  2017-08-15 03:54:13.165000      TK_PLCS/Pixel/crate03/module09/channel_read_05              
PixelEndcap BmO PC 1A                               TAir        PixelEndCap_BmO       -10.894097  2017-08-15 03:58:03.161000      TK_PLCS/Pixel/crate03/module10/channel_read_01              
PixelEndcap BmO PC 1B                               TAir        PixelEndCap_BmO       -10.199652  2017-08-15 03:56:20.665000      TK_PLCS/Pixel/crate03/module10/channel_read_02              
PixelEndcap BmO PC 1C                               TAir        PixelEndCap_BmO       -10.416666  2017-08-15 03:51:49.170000      TK_PLCS/Pixel/crate03/module10/channel_read_03              
PixelEndcap BmO PC 1D                               TAir        PixelEndCap_BmO       -9.6354166  2017-08-15 03:54:13.165000      TK_PLCS/Pixel/crate03/module10/channel_read_04              
PixelEndcap BmO PC 2A                               TAir        PixelEndCap_BmO       -10.980902  2017-08-15 03:55:25.660000      TK_PLCS/Pixel/crate03/module10/channel_read_05              
PixelEndcap BmO PC 2B                               TAir        PixelEndCap_BmO       -11.154513  2017-08-15 03:55:48.256000      TK_PLCS/Pixel/crate03/module10/channel_read_06              
PixelEndcap BmO PC 2C                               TAir        PixelEndCap_BmO       -10.329861  2017-08-15 03:52:05.675000      TK_PLCS/Pixel/crate03/module10/channel_read_07              
PixelEndcap BmO PC 3A                               TAir        PixelEndCap_BmO       -9.765625   2017-08-15 03:52:03.164000      TK_PLCS/Pixel/crate03/module11/channel_read_01              
PixelEndcap BmO PC 3B                               TAir        PixelEndCap_BmO       -11.111111  2017-08-15 03:58:03.161000      TK_PLCS/Pixel/crate03/module11/channel_read_02              
PixelEndcap BmO PC 3C                               TAir        PixelEndCap_BmO       -10.633680  2017-08-15 03:53:00.663000      TK_PLCS/Pixel/crate03/module11/channel_read_03              
PixelEndcap BmO PC 3D                               TAir        PixelEndCap_BmO       -8.2465277  2017-08-15 03:52:48.255000      TK_PLCS/Pixel/crate03/module11/channel_read_04              
PixelEndcap BmO Air                                 TAir        PixelEndCap_BmO       -7.03125    2017-08-15 03:57:45.669000      TK_PLCS/Pixel/crate03/module11/channel_read_05              
PixelBarrel BpI 6I L4D3PN                           TLiq        PixelBarrel_BpI       -13.69      2017-08-15 03:51:40.673000      TK_PLCS/Pixel/crate04/module04/channel_read_07              
PixelBarrel BpI 6M L4D3PN                           TLiq        PixelBarrel_BpI       -11.85      2017-08-15 03:51:49.170000      TK_PLCS/Pixel/crate04/module04/channel_read_08              
PixelBarrel BpI 6R L4D3PN                           TLiq        PixelBarrel_BpI       -11.14      2017-08-15 03:51:43.157000      TK_PLCS/Pixel/crate04/module05/channel_read_01              
PixelBarrel BpI Sector 5 POH 1R Detector            TSil        PixelBarrel_BpI       13.35       2017-08-15 04:00:15.666000      TK_PLCS/Pixel/crate04/module05/channel_read_02              
PixelBarrel BpI Sector 6 POH 2R Inp                 TSil        PixelBarrel_BpI       -9.24       2017-08-14 08:32:28.167000      TK_PLCS/Pixel/crate04/module05/channel_read_03              
PixelBarrel BpI Sector 6 POH 2R Detector            TSil        PixelBarrel_BpI       13.89       2017-08-15 03:57:43.165000      TK_PLCS/Pixel/crate04/module05/channel_read_04              
PixelBarrel BpI Sector 7 POH 3R Inp                 TSil        PixelBarrel_BpI       -9.37       2017-08-15 03:58:58.171000      TK_PLCS/Pixel/crate04/module05/channel_read_05              
PixelBarrel BpI Sector 7 POH 3R Detector            TSil        PixelBarrel_BpI       12.54       2017-08-15 03:58:40.672000      TK_PLCS/Pixel/crate04/module05/channel_read_06              
PixelBarrel BpI Sector 8 POH 4R Inp                 TSil        PixelBarrel_BpI       -9.97       2017-08-15 03:52:50.680000      TK_PLCS/Pixel/crate04/module05/channel_read_07              
PixelBarrel BpI Sector 8 POH 4R Detector            TSil        PixelBarrel_BpI       9.86        2017-08-15 03:57:53.172000      TK_PLCS/Pixel/crate04/module05/channel_read_08              
PixelBarrel BpO 1I L4D2PF                           TLiq        PixelBarrel_BpO       -10.87      2017-08-15 03:52:30.677000      TK_PLCS/Pixel/crate04/module06/channel_read_07              
PixelBarrel BpO 1M L4D2PF                           TLiq        PixelBarrel_BpO       -11.31      2017-08-14 21:34:15.669000      TK_PLCS/Pixel/crate04/module06/channel_read_08              
PixelBarrel BpO Sector 4 POH 1R Detector            TSil        PixelBarrel_BpO       6.21        2017-08-15 03:59:25.657000      TK_PLCS/Pixel/crate04/module07/channel_read_02              
PixelBarrel BpO Sector 3 POH 2R Inp                 TSil        PixelBarrel_BpO       -9.12       2017-08-14 21:38:10.668000      TK_PLCS/Pixel/crate04/module07/channel_read_03              
PixelBarrel BpO Sector 3 POH 2R Detector            TSil        PixelBarrel_BpO       13.42       2017-08-15 03:59:38.174000      TK_PLCS/Pixel/crate04/module07/channel_read_04              
PixelBarrel BpO Sector 2 POH 3R Inp                 TSil        PixelBarrel_BpO       -8.37       2017-08-15 03:56:18.273000      TK_PLCS/Pixel/crate04/module07/channel_read_05              
PixelBarrel BpO Sector 2 POH 3R Detector            TSil        PixelBarrel_BpO       17.16       2017-08-15 04:00:35.662000      TK_PLCS/Pixel/crate04/module07/channel_read_06              
PixelBarrel BpO Sector 1 POH 4R Inp                 TSil        PixelBarrel_BpO       -9.34       2017-08-14 21:39:40.668000      TK_PLCS/Pixel/crate04/module07/channel_read_07              
PixelBarrel BpO Sector 1 POH 4R Detector            TSil        PixelBarrel_BpO       17.19       2017-08-15 04:00:23.167000      TK_PLCS/Pixel/crate04/module07/channel_read_08              
PixelBarrel BmI 1I L4D2MN                           TLiq        PixelBarrel_BmI       -10.7       2017-08-15 03:51:38.163000      TK_PLCS/Pixel/crate04/module08/channel_read_07              
PixelBarrel BmI 1M L4D2MN                           TLiq        PixelBarrel_BmI       -11.84      2017-08-15 03:51:43.157000      TK_PLCS/Pixel/crate04/module08/channel_read_08              
PixelBarrel BmI 1R L4D2MN                           TLiq        PixelBarrel_BmI       -14.38      2017-08-15 03:52:45.659000      TK_PLCS/Pixel/crate04/module09/channel_read_01              
PixelBarrel BmI Sector 4 POH 1R Detector            TSil        PixelBarrel_BmI       11.28       2017-08-15 03:59:20.659000      TK_PLCS/Pixel/crate04/module09/channel_read_02              
PixelBarrel BmI Sector 3 POH 2R Inp                 TSil        PixelBarrel_BmI       -9.29       2017-08-15 03:56:30.671000      TK_PLCS/Pixel/crate04/module09/channel_read_03              
PixelBarrel BmI Sector 3 POH 2R Detector            TSil        PixelBarrel_BmI       15.74       2017-08-15 03:59:23.164000      TK_PLCS/Pixel/crate04/module09/channel_read_04              
PixelBarrel BmI Sector 2 POH 3R Inp                 TSil        PixelBarrel_BmI       -8.27       2017-08-15 03:59:43.158000      TK_PLCS/Pixel/crate04/module09/channel_read_05              
PixelBarrel BmI Sector 2 POH 3R Detector            TSil        PixelBarrel_BmI       18.14       2017-08-15 03:58:15.668000      TK_PLCS/Pixel/crate04/module09/channel_read_06              
PixelBarrel BmI Sector 1 POH 4R Inp                 TSil        PixelBarrel_BmI       -9.22       2017-08-15 03:54:48.263000      TK_PLCS/Pixel/crate04/module09/channel_read_07              
PixelBarrel BmI Sector 1 POH 4R Detector            TSil        PixelBarrel_BmI       18.14       2017-08-15 04:00:28.284000      TK_PLCS/Pixel/crate04/module09/channel_read_08              
PixelBarrel BmO 6I L4D3MF                           TLiq        PixelBarrel_BmO       -12.38      2017-08-15 03:52:38.164000      TK_PLCS/Pixel/crate04/module10/channel_read_07              
PixelBarrel BmO 6M L4D3MF                           TLiq        PixelBarrel_BmO       -12.86      2017-08-14 08:17:28.166000      TK_PLCS/Pixel/crate04/module10/channel_read_08              
PixelBarrel BmO 6R L4D3MF                           TLiq        PixelBarrel_BmO       -13.78      2017-08-15 03:53:48.271000      TK_PLCS/Pixel/crate04/module11/channel_read_01              
PixelBarrel BmO Sector 5 POH 1R Detector            TSil        PixelBarrel_BmO       7.33        2017-08-15 03:59:28.162000      TK_PLCS/Pixel/crate04/module11/channel_read_02              
PixelBarrel BmO Sector 6 POH 2R Inp                 TSil        PixelBarrel_BmO       -10.02      2017-08-15 03:58:13.163000      TK_PLCS/Pixel/crate04/module11/channel_read_03              
PixelBarrel BmO Sector 6 POH 2R Detector            TSil        PixelBarrel_BmO       13.08       2017-08-15 03:58:20.664000      TK_PLCS/Pixel/crate04/module11/channel_read_04              
PixelBarrel BmO Sector 7 POH 3R Inp                 TSil        PixelBarrel_BmO       -9.12       2017-08-15 03:53:38.169000      TK_PLCS/Pixel/crate04/module11/channel_read_05              
PixelBarrel BmO Sector 7 POH 3R Detector            TSil        PixelBarrel_BmO       12.11       2017-08-15 03:59:58.163000      TK_PLCS/Pixel/crate04/module11/channel_read_06              
PixelBarrel BmO Sector 8 POH 4R Inp                 TSil        PixelBarrel_BmO       -10.6       2017-08-14 06:29:18.265000      TK_PLCS/Pixel/crate04/module11/channel_read_07              
PixelBarrel BmO Sector 8 POH 4R Detector            TSil        PixelBarrel_BmO       9.11        2017-08-15 03:59:43.158000      TK_PLCS/Pixel/crate04/module11/channel_read_08              
PixelBarrel BpI 1M L4D1PN                           TLiq        PixelBarrel_BpI       -10.87      2017-08-15 03:51:40.673000      TK_PLCS/Pixel/crate05/module04/channel_read_08              
PixelBarrel BpI 1R L4D1PN                           TLiq        PixelBarrel_BpI       -13.97      2017-08-15 03:53:48.271000      TK_PLCS/Pixel/crate05/module05/channel_read_01              
PixelBarrel BpI Sector 4 POH 1L Detector            TSil        PixelBarrel_BpI       11.54       2017-08-15 03:57:30.665000      TK_PLCS/Pixel/crate05/module05/channel_read_02              
PixelBarrel BpI Sector 3 POH 2L Inp                 TSil        PixelBarrel_BpI       -8.9        2017-08-15 03:56:58.165000      TK_PLCS/Pixel/crate05/module05/channel_read_03              
PixelBarrel BpI Sector 3 POH 2L Detector            TSil        PixelBarrel_BpI       15.35       2017-08-15 03:59:40.666000      TK_PLCS/Pixel/crate05/module05/channel_read_04              
PixelBarrel BpI Sector 2 POH 3L Inp                 TSil        PixelBarrel_BpI       -7.55       2017-08-15 03:53:20.671000      TK_PLCS/Pixel/crate05/module05/channel_read_05              
PixelBarrel BpI Sector 2 POH 3L Detector            TSil        PixelBarrel_BpI       18.21       2017-08-15 03:59:15.655000      TK_PLCS/Pixel/crate05/module05/channel_read_06              
PixelBarrel BpI Sector 1 POH 4L Inp                 TSil        PixelBarrel_BpI       -9.34       2017-08-15 03:59:58.163000      TK_PLCS/Pixel/crate05/module05/channel_read_07              
PixelBarrel BpI Sector 1 POH 4L Detector            TSil        PixelBarrel_BpI       18.65       2017-08-15 03:59:28.162000      TK_PLCS/Pixel/crate05/module05/channel_read_08              
PixelBarrel BpO 6I L4D4PF                           TLiq        PixelBarrel_BpO       -12.86      2017-08-15 03:48:20.668000      TK_PLCS/Pixel/crate05/module06/channel_read_07              
PixelBarrel BpO 6M L4D4PF                           TLiq        PixelBarrel_BpO       -11.28      2017-08-15 03:52:30.677000      TK_PLCS/Pixel/crate05/module06/channel_read_08              
PixelBarrel BpO 6R L4D4PF                           TLiq        PixelBarrel_BpO       -9.54       2017-08-15 03:52:53.172000      TK_PLCS/Pixel/crate05/module07/channel_read_01              
PixelBarrel BpO Sector 5 POH 1L Detector            TSil        PixelBarrel_BpO       10.47       2017-08-15 03:59:08.158000      TK_PLCS/Pixel/crate05/module07/channel_read_02              
PixelBarrel BpO Sector 6 POH 2L Inp                 TSil        PixelBarrel_BpO       -9.18       2017-08-14 21:38:03.158000      TK_PLCS/Pixel/crate05/module07/channel_read_03              
PixelBarrel BpO Sector 6 POH 2L Detector            TSil        PixelBarrel_BpO       12.65       2017-08-15 03:59:20.659000      TK_PLCS/Pixel/crate05/module07/channel_read_04              
PixelBarrel BpO Sector 7 POH 3L Inp                 TSil        PixelBarrel_BpO       -9.07       2017-08-15 03:52:53.172000      TK_PLCS/Pixel/crate05/module07/channel_read_05              
PixelBarrel BpO Sector 7 POH 3L Detector            TSil        PixelBarrel_BpO       12.84       2017-08-15 03:59:38.174000      TK_PLCS/Pixel/crate05/module07/channel_read_06              
PixelBarrel BpO Sector 8 POH 4L Detector            TSil        PixelBarrel_BpO       9.49        2017-08-15 04:00:43.171000      TK_PLCS/Pixel/crate05/module07/channel_read_08              
PixelBarrel BmI 6R L4D4MN                           TLiq        PixelBarrel_BmI       -10.87      2017-08-15 03:51:40.673000      TK_PLCS/Pixel/crate05/module08/channel_read_07              
PixelBarrel BmI 6M L4D4MN                           TLiq        PixelBarrel_BmI       -11.99      2017-08-15 03:51:45.667000      TK_PLCS/Pixel/crate05/module08/channel_read_08              
PixelBarrel BmI 6I L4D4MN                           TLiq        PixelBarrel_BmI       -14.04      2017-08-15 03:51:35.667000      TK_PLCS/Pixel/crate05/module09/channel_read_01              
PixelBarrel BmI Sector 5 POH 1L Detector            TSil        PixelBarrel_BmI       10.91       2017-08-15 03:57:28.166000      TK_PLCS/Pixel/crate05/module09/channel_read_02              
PixelBarrel BmI Sector 6 POH 2L Inp                 TSil        PixelBarrel_BmI       -9.22       2017-08-15 03:52:45.659000      TK_PLCS/Pixel/crate05/module09/channel_read_03              
PixelBarrel BmI Sector 6 POH 2L Detector            TSil        PixelBarrel_BmI       11.84       2017-08-15 03:58:38.292000      TK_PLCS/Pixel/crate05/module09/channel_read_04              
PixelBarrel BmI Sector 7 POH 3L Inp                 TSil        PixelBarrel_BmI       -9.02       2017-08-15 03:53:18.273000      TK_PLCS/Pixel/crate05/module09/channel_read_05              
PixelBarrel BmI Sector 7 POH 3L Detector            TSil        PixelBarrel_BmI       11.28       2017-08-15 04:00:38.168000      TK_PLCS/Pixel/crate05/module09/channel_read_06              
PixelBarrel BmI Sector 8 POH 4L Inp                 TSil        PixelBarrel_BmI       -10.26      2017-08-14 06:29:00.664000      TK_PLCS/Pixel/crate05/module09/channel_read_07              
PixelBarrel BmI Sector 8 POH 4L Detector            TSil        PixelBarrel_BmI       9.33        2017-08-15 03:57:55.676000      TK_PLCS/Pixel/crate05/module09/channel_read_08              
PixelBarrel BmO 1I L4D1MF                           TLiq        PixelBarrel_BmO       -9.87       2017-08-15 03:51:45.667000      TK_PLCS/Pixel/crate05/module10/channel_read_07              
PixelBarrel BmO 1M L4D1MF                           TLiq        PixelBarrel_BmO       -10.84      2017-08-15 03:51:43.157000      TK_PLCS/Pixel/crate05/module10/channel_read_08              
PixelBarrel BmO 1R L4D1MF                           TLiq        PixelBarrel_BmO       -13.68      2017-08-15 03:51:53.166000      TK_PLCS/Pixel/crate05/module11/channel_read_01              
PixelBarrel BmO Sector 4  POH 1L Detector           TSil        PixelBarrel_BmO       10.69       2017-08-15 03:59:45.662000      TK_PLCS/Pixel/crate05/module11/channel_read_02              
PixelBarrel BmO Sector 3 POH 2L Inp                 TSil        PixelBarrel_BmO       -8.95       2017-08-15 03:52:55.656000      TK_PLCS/Pixel/crate05/module11/channel_read_03              
PixelBarrel BmO Sector 3 POH 2L Detector            TSil        PixelBarrel_BmO       18.11       2017-08-15 03:59:05.666000      TK_PLCS/Pixel/crate05/module11/channel_read_04              
PixelBarrel BmO Sector 2 POH 3L Inp                 TSil        PixelBarrel_BmO       -8.88       2017-08-15 03:57:00.664000      TK_PLCS/Pixel/crate05/module11/channel_read_05              
PixelBarrel BmO Sector 2 POH 3L Detector            TSil        PixelBarrel_BmO       19.35       2017-08-15 04:00:25.666000      TK_PLCS/Pixel/crate05/module11/channel_read_06              
PixelBarrel BmO Sector 1 POH 4L Inp                 TSil        PixelBarrel_BmO       -9.34       2017-08-15 03:53:00.663000      TK_PLCS/Pixel/crate05/module11/channel_read_07              
PixelBarrel BmO Sector 1 POH 4L Detector            TSil        PixelBarrel_BmO       19.38       2017-08-15 03:59:08.158000      TK_PLCS/Pixel/crate05/module11/channel_read_08              
