import cx_Oracle
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import sql3 as sql
import sys
###from numberOfROCs import numberOfRocs

if __name__ == "__main__":
            db = "temps.db"
            tb = "temperatures"
            if len(sys.argv) > 1:
                starttime = sys.argv[1]
                #stoptime = datetime.datetime(starttime make starttime + 3h?
            if len(sys.argv) > 2:
                stoptime = sys.argv[2]
            else:
                print "usage e.g."
                starttime = "2017-10-05 13:00:45.421363"
                stoptime = "2017-10-06 05:00:45.421363"
                print "python getTemps.py", "'" + starttime + "' '" + stoptime + "'"
                sys.exit()
            ### Opening connection
            connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_adg') # offline
            # connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_lb') # online
            cursor = connection.cursor()
            cursor.arraysize=50


            headers = ['part', 'alias', 'since', 'update_count', 'value_converted', 'change_date', 'dpid', 'dpname' ]
            #print 'create table temperatures (' + ''.join([h + ' text, ' for h in headers]) + 'unique(' + ''.join([h + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
            print starttime
            print stoptime

            ### Define query to get temperatures for Barrel Pixels
            query2="""
with IDs as ( select id, alias, since, rtrim(dpe_name,'.') as dp, substr(alias,instr(alias,'/',-1)+1) as part, update_count, CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname as dpname from CMS_TRK_DCS_PVSS_COND.aliases, CMS_TRK_DCS_PVSS_COND.dp_name2id where CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname like  '%Pixel%' and  rtrim(CMS_TRK_DCS_PVSS_COND.aliases.dpe_name,'.') = CMS_TRK_DCS_PVSS_COND.dp_name2id.dpname)
select part, alias, since, update_count, value_converted, change_date , dpid, dpname from CMS_TRK_DCS_PVSS_COND.tkplcreadsensor, IDs where IDs.id = CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.DPID and CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date >= to_timestamp('""" + str(starttime) + """', 'RRRR-MM-DD HH24:MI:SS.FF') AND CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date < to_timestamp('""" + str(stoptime) + """', 'RRRR-MM-DD HH24:MI:SS.FF') order by part, CMS_TRK_DCS_PVSS_COND.tkplcreadsensor.change_date
"""

            print query2
            cursor.execute(query2)
            #{"the_start_time" : starttime, "the_end_time" : stoptime})
            row = cursor.fetchall()
#
            fileCurrents = "temps.txt"
            fcur = open(fileCurrents, "w+")
            #print headers

            for i in xrange(len(row)):
                #print len(row)
                #print "====> ", "".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n"
                fcur.write("".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n")
                vals = row[i]
                #print vals

                values = {}
                for i in range(len(headers)):
                    if type(vals[i]) == str:
                        values[headers[i]] =  vals[i].strip('cms_trk_dcs_4:')
                    else:
                        values[headers[i]] =  vals[i]
                #print "writing to db", values
                if values['value_converted'] != None:
                    print values['value_converted'], 'found at', datetime.datetime.isoformat(values['change_date']), "for", values['dpid'], values['alias']
                sql.dict_to_db(values, db, tb)

            fcur.close()


            connection.close()





