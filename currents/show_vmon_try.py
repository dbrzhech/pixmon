import cx_Oracle
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import sql3 as sql
import sys
import argparse
###from numberOfROCs import numberOfRocs


def vmon_imon(starttime, stoptime, channel='', layers='', disk='', quantity='vmon'):
    """
    Query database to get vmon and imon between starttime and stoptime.
    channel can be:
        0: digital current
        1: analog current
        2: layer 3 for L23 or layer 1 for L14
        3: layer 2 for L23 or layer 4 for L14
    layers can be
        14: layers 1 and 4
        23: layers 2 and 3
    """
    ### Opening connection
    connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_adg') # offline
    # connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_lb') # online
    cursor = connection.cursor()
    cursor.arraysize=50
    layerdisk = 'LAY'
    barrelendcap = ''
    if disk != '':
        barrelendcap = 'EndCap'
        layerdisk = ''
        layers = disk
    options = """'%Pixel""" + str(barrelendcap) + """%%""" + layerdisk + str(layers) + """%%channel00""" + str(channel) + """%'"""
    # Each pixel LAYER/ROG has a unique dpid. Using dpid directly in the query of currents can save plenty of time!
    dpid_rows_query ="""select dpe_name, alias, id from cms_trk_dcs_pvss_cond.aliases join cms_trk_dcs_pvss_cond.dp_name2id on (dpname || '.' = dpe_name) where alias like """ + options
    cursor.execute(dpid_rows_query)
    dpid_rows = cursor.fetchall()
    rows = {}
    quantity = quantity[0].upper() + quantity[1:].lower()

    ### Define query to get temperatures for Barrel Pixels
    for k in range(len(dpid_rows))[:]:
        moduleName = str(dpid_rows[k][1])
        moduleName = moduleName[59:]
        dpidname = dpid_rows[k][2]
        query = """select actual_""" + quantity + """, change_date from cms_trk_dcs_pvss_cond.fwcaenchannel         where change_date between TO_TIMESTAMP('""" + starttime + """', 'RRRR-MM-DD HH24:MI:SS.FF') and TO_TIMESTAMP('""" + stoptime + """', 'RRRR-MM-DD HH24:MI:SS.FF') and dpid='""" + str(dpidname) + """' and actual_""" + quantity + """ is not NULL         order by change_date"""

        cursor.execute(query)
        rows[moduleName] = cursor.fetchall()
    connection.close()

    return rows


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Get currents and voltages from database.')
    parser.add_argument('--minutes', '-m', dest='minutes', default=1, help='Number of minutes to go back.')
    parser.add_argument('--hours', '-o', dest='hours', default=1, help='Number of hours to go back.')
    parser.add_argument('--seconds', '-s', dest='seconds', default=1, help='Number of seconds to go back.')
    parser.add_argument('--starttime', '-i', dest='starttime', help='Set start time for query, e.g. --starttime "2018-04-18 18:00:00.0".')
    parser.add_argument('--stoptime', '-f', dest='stoptime', help='Set stop time for query, e.g. --stoptime "2018-04-18 18:00:00.0".')
    parser.add_argument('--layer', '-l', dest='layer', help='Select a layer, e.g. 1, 2, 3, 4.')
    parser.add_argument('--disk', '-d', dest='disk', help='Select a disk, e.g. 1, 2, 3.')
    parser.add_argument('--analog', '-a', dest='analog', action='store_true', help='Get low voltage analog current current.')
    parser.add_argument('--digital', '-g', dest='digital', action='store_true', help='Get low voltage digital current current.')
    parser.add_argument('--quantity', '-q', dest='quantity', help='Select a quantity, e.g. vmon, imon.')
    args = parser.parse_args()

    db = "currents.db"
    tb = "currents"
    amtseconds = 0
    amtminutes = 10
    amthours = 0
    quantity = 'vmon'
    if args.hours != None: amthours = int(args.hours)
    if args.seconds != None: amtseconds = int(args.seconds)
    if args.minutes != None: amtminutes = int(args.minutes)
    if args.starttime != None: starttime = args.starttime
    if args.stoptime != None: stoptime = args.stoptime
    powersupply = {1: '14', 2: '23', 3: '23', 4: '14'}
    hvchannel = {1: '1', 2: '2', 3: '1', 4: '2'}
    channel = ''
    layers = ''
    disk = ''
    if args.layer != None:
        layers = int(powersupply[int(args.layer)])
        channel = int(hvchannel[int(args.layer)]) + 1
    if args.disk != None:
        disk = int(args.disk)
    if args.analog:
        channel = 1
        quantity = 'imon'
    if args.digital:
        channel = 0
        quantity = 'imon'



    stoptime = datetime.datetime.isoformat(datetime.datetime.utcnow()).replace('T', ' ')
    starttime = datetime.datetime.isoformat(datetime.datetime.utcnow() - datetime.timedelta(minutes=amtminutes, seconds=amtseconds, hours=amthours)).replace('T', ' ')
    print "getting data for last", amtminutes, "minute(s) and ", amtseconds, "second(s)"


    headers = [quantity, 'change_date' ]
    print starttime
    print stoptime



    currents_voltages = vmon_imon(starttime, stoptime, layers=layers, disk=disk, channel=channel, quantity=quantity)
    for modname in currents_voltages:
        print 'sector:', modname
        for vals in currents_voltages[modname]:

            #print vals
            values = {}
            for i in range(len(headers)):
                #print vals
                #if type(vals[i]) == str:
                #    values[headers[i]] =  vals[i].strip('cms_trk_dcs_4:')
                #else:
                #    values[headers[i]] =  vals[i]
                values[headers[i]] = vals[i]
            if values[quantity] != None:
                print quantity, values[quantity], 'found at', datetime.datetime.isoformat(values['change_date']), "for", modname
            sql.dict_to_db(values, db, tb)








