#!/usr/bin/env zsh
import sql3 as sql
import sys

db = "currents.db"
tb = "currents"
if __name__ == "__main__":
    if len(sys.argv) > 1:
        names = open(sys.argv[1], 'r')
    else:
        names = open('currents.txt', 'r')
    line = names.readline()
    headers = ['detector', 'sector', 'layerrog', 'quantity', 'current', 'change_date', 'alias', 'channel', 'detpart']
    print 'create table currents (' + ''.join([h + ' text, ' for h in headers]) + 'unique(' + ''.join([h + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
    stop = 0
    maxlines = 3000
    maxlines = 13
    line = names.readline()
    channels = {'channel000': 'dig', 'channel001': 'ana', 'channel002': 'HV1', 'channel003': 'HV2'}
    while line != '': #and stop < maxlines:
        #vals = line.split(',')
        vals = line.split()
        values = {}
        values['alias'] = vals[0]
        values['detpart'] = values['alias'].split('/')[0]
        values['channel'] = values['alias'].split('/')[1]
        values['quantity'] = channels[values['channel']]
        values['detector'] = values['detpart'].split('_')[0]
        values['sector'] = values['detpart'].split('_')[1]
        values['sector'] += values['detpart'].split('_')[2][1:]
        values['layerrog'] = values['detpart'].split('_')[3]
        values['current'] = vals[1]
        values['change_date'] = '"' + vals[3] + ' ' + vals[4] + '"'

        values = {header: values[header] for header in headers}
        stop += 1
        sql.dict_to_db(values, db, tb)
        line = names.readline()