import cx_Oracle
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
###from numberOfROCs import numberOfRocs

if __name__ == "__main__":
            ### Opening connection
            connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_adg')
            # connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_lb')
            cursor = connection.cursor()
            cursor.arraysize=50

    ### Define query to get currents for Barrel Pixels


            query2="""
with cables as (
  select distinct substr(lal.alias,INSTR(lal.alias,  '/', -1, 2)+1)  cable, id dpid, cd from
    (select max(since) as cd, alias from  cms_trk_dcs_pvss_cond.aliases group by alias ) md, cms_trk_dcs_pvss_cond.aliases lal
  join cms_trk_dcs_pvss_cond.dp_name2id on dpe_name=concat(dpname,'.')
  where md.alias=lal.alias and lal.since=cd
  and (lal.alias like 'CMS_TRACKER/%Pixel%/channel00%')
),
it as (
  select dpid, max(change_date) itime from cms_trk_dcs_pvss_cond.fwcaenchannel caen
  WHERE change_date between to_timestamp('2017-06-29 02:00:45.421363', 'RRRR-MM-DD HH24:MI:SS.FF') and to_timestamp('2017-06-30 04:00:45.421363', 'RRRR-MM-DD HH24:MI:SS.FF')
  AND actual_Imon is not NULL
  group by dpid
),
i_mon as (
  select it.dpid, itime, actual_Imon, actual_Vmon from cms_trk_dcs_pvss_cond.fwcaenchannel caen
  join it on (it.dpid = caen.dpid and change_date = itime)
  and actual_Imon is not NULL
)
select cable, actual_Imon, actual_Vmon, itime from i_mon
join cables on (i_mon.dpid=cables.dpid)
order by itime
"""

            print query2
            cursor.execute(query2)
            #{"the_start_time" : begintime, "the_end_time" : endtime})
            row = cursor.fetchall()
#
            fileCurrents = "currents.txt"
            fcur = open(fileCurrents, "w+")

            for i in xrange(len(row)):
                #print len(row)
                print "====> ", "".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n"
                fcur.write("".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n")
                #fcur.write(str(row[i][0]) + "   " + str(row[i][1]) + "   " + str(row[i][2])+ "\n")

            fcur.close()


            connection.close()





