#!/usr/bin/env zsh
import sql3 as sql
import sys

db = "currents.db"
tb = "currents_vs_lumi"
tbinterfill = "currents_interfill"
tbfills = "fills"
tbcurrents = "currents"
import datetime



def string_to_datetime(s):
    if '.' in s:
        return datetime.datetime.strptime(s.strip('"'), "%Y-%m-%d %H:%M:%S.%f")
    else:
        return datetime.datetime.strptime(s.strip('"'), "%Y-%m-%d %H:%M:%S")

if __name__ == "__main__":
    fillnos = range(10000)
    #fillnos = range(5162, 5170)
    if len(sys.argv) > 1:
        names = open(sys.argv[1], 'r')
    else:
        names = open('fills.txt', 'r')
    line = names.readline()
    headers_currents = ['detector', 'sector', 'layerrog', 'quantity', 'current', 'change_date', 'alias', 'channel', 'detpart']
    headers_fills = ['fill', 'begin', 'end', 'intlumi']
    headers = ['detector', 'sector', 'layerrog', 'quantity', 'current', 'changedate', 'fill', 'intlumi']
    print 'create table currents_vs_lumi (' + ''.join([h + ' text, ' for h in headers]) + 'unique(' + ''.join([h + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
    print 'create table currents_interfill (' + ''.join([h + ' text, ' for h in headers]) + 'unique(' + ''.join([h + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
    print 'create table fills (' + ''.join([h + ' text, ' for h in headers_fills]) + 'unique(' + ''.join([h + ', ' for h in headers_fills]) + ' ) on conflict replace);'
    stop = 0
    maxlines = 3000
    maxlines = 1
    line = names.readline()
    channels = {'channel000': 'dig', 'channel001': 'ana', 'channel002': 'HV1', 'channel003': 'HV2'}
    fills = sql.dict_from_dicts(sql.dicts_from_db_table(db, tbfills), ['fill'])
    currents = sql.dicts_from_db_table(db, tbcurrents)
    for current in currents: #[:maxlines]:
        for fillno in fillnos:
            if fillno not in fills: continue
            fill = fills[fillno]
            fillbegin = datetime.datetime.strptime(fill['begin'].strip('"'), "%Y-%m-%d %H:%M:%S.%f")
            fillend = datetime.datetime.strptime(fill['end'].strip('"'), "%Y-%m-%d %H:%M:%S.%f")
            currentdate = datetime.datetime.strptime(current['change_date'].strip('"'), "%Y-%m-%d %H:%M:%S.%f")
            print ''
            #print fillbegin, currentdate, fillend
            #print current
            if fillbegin < currentdate <= fillend:
                current.update(fill)
                print 'in a fill:', current
                sql.dict_to_db(current, db, tb)
            #elif currentdate > fillend:
            #    not_in_fills = True
            #    fillno_next = fillno + 1
            #    while not_in_fills:
            #        if fillno_next in fills:
            #            not_in_fills = False
            #            next_fill_start = string_to_datetime(fills[fillno_next]['begin'])
            #        fillno_next += 1
            #    if currentdate < next_fill_start:
            #        current.update(fill)
            #        print 'to interfill:', current
            #        sql.dict_to_db(current, db, tbinterfill)
            #        continue
        stop += 1
        line = names.readline()
