import numpy as np
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
import math



if __name__ == "__main__":
    fig = plt.figure(figsize=(7.5,6))
    xmin = 0
    xmax = 400
    ymin = 0
    ymax = 400
    grid_x, grid_y = np.mgrid[0:1500:151j, 0:1000:150j]
    plt.axis([xmin, xmax, ymin, ymax])
    colors = ['m', 'b', 'orange', 'orange']
    linestyles = [':', '--', '--']
    i = 1
    linestyle = linestyles[i]
    linecolor = colors[i]
    xyz = [(1, 1, 1), (1, 2, 3), (1, 3, 1.5), (1, 4, 2.33)]
    xyz.extend([(2, 2, 1), (2, 2, 3), (2, 3, 1.5), (2, 4, 2.33)])
    xyz.extend([(3, 3, 1), (3, 2, 2), (3, 3, np.nan), (3, 4, 2.33)])
    xyz.extend([(4, 4, 1), (4, 2, np.nan), (4, 3, 1.23), (4, 4, 1.5)])

    x, y, z = zip(*xyz)
    plt.hist2d(x, y, weights=z)
    print x, y, z
    #zi = griddata(x,y,z,grid_x,grid_y,interp='linear')
    #cont = plt.contour(grid_x, grid_y, zi,[20], linewidths=5, linestyles=linestyle, colors=linecolor)
    plt.annotate(r'$\deg$C', (0.05,0.9), xycoords='axes fraction', fontsize=26, color='black')
    plt.tick_params(labelsize=16)
    #plt.xticks(np.arange(xmin, xmax+100, 100))
    #plt.yticks(np.arange(ymin, ymax + 100, 100))
    dest_title = 'plotforfeng.png'
    plt.savefig(dest_title, bbox_inches='tight')
    print 'Saved figure to ', dest_title
    plt.close()

