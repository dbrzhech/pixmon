import cx_Oracle
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
###from numberOfROCs import numberOfRocs

if __name__ == "__main__":
            ### Opening connection
            connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_adg')
            # connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_lb')
            cursor = connection.cursor()
            cursor.arraysize=50

    ### Define query to get currents for Barrel Pixels


            query2="""
with i_lumi as (
  select lhcfill fn, integratedlumi il from CMS_RUNTIME_LOGGER.RUNTIME_SUMMARY
  where BEGINTIME IS NOT NULL and lhcfill is not null and (lhcfill < 3474 or lhcfill > 3564) and integratedlumi is not null and integratedlumi > 0 
  order by lhcfill
),

good_fills as (
  select lhcfill, begintime, endtime, integratedlumi from CMS_RUNTIME_LOGGER.RUNTIME_SUMMARY
  where BEGINTIME IS NOT NULL and lhcfill is not null  and (lhcfill < 3474 or lhcfill > 3564) and integratedlumi is not null and integratedlumi > 0 
  order by lhcfill
)

select good_fills.lhcfill, begintime, endtime, sum(il) totlumi from good_fills, i_lumi
 where good_fills.lhcfill>fn
 group by good_fills.lhcfill, good_fills.begintime, good_fills.endtime
 order by good_fills.lhcfill
"""

            print query2
            cursor.execute(query2)
            #{"the_start_time" : begintime, "the_end_time" : endtime})
            row = cursor.fetchall()
#
            fileCurrents = "fills.txt"
            fcur = open(fileCurrents, "w+")

            for i in xrange(len(row)):
                #print len(row)
                print "====> ", "".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n"
                fcur.write("".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n")
                #fcur.write(str(row[i][0]) + "   " + str(row[i][1]) + "   " + str(row[i][2])+ "\n")

            fcur.close()


            connection.close()





