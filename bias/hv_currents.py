testwithoutdb = False
if not testwithoutdb:
    import cx_Oracle
import datetime
import glob
import matplotlib.pyplot as plt
import socket
import matplotlib.dates as dates
import sql3 as sql
import sys
import argparse
###from numberOfROCs import numberOfRocs


def vmon_imon(starttime, stoptime, channel='', layers='', disk='', halfshell='', sectorsrogs='', quantity='vmon', testwithoutdb=False):
    """
    Query database to get vmon and imon between starttime and stoptime.
    channel can be:
        0: digital current
        1: analog current
        2: layer 3 for L23 or layer 1 for L14
        3: layer 2 for L23 or layer 4 for L14
    layers can be
        14: layers 1 and 4
        23: layers 2 and 3
    """
    if not testwithoutdb:
        ### Opening connection
        connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_adg') # offline
        # connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_lb') # online
        cursor = connection.cursor()
        cursor.arraysize=50
    layerdisk = ''
    barrelendcap = ''
    if disk != '':
        barrelendcap = 'EndCap'
        layerdisk = ''
        layers = disk
    elif layers != '':
        layerdisk = 'LAY'
    rog = ''
    sector = ''
    if sectorsrogs != '':
        if barrelendcap != 'EndCap':
            sector = 'S' + str(sectorsrogs)
        else:
            rog = 'ROG' + str(sectorsrogs)


    options = """'%Pixel""" + str(barrelendcap) + """%%""" + halfshell + """%%""" + str(sector) + """%%""" + layerdisk + str(layers) + """%%""" + str(rog) + """%%channel00""" + str(channel) + """%'"""
    # Each pixel LAYER/ROG has a unique dpid. Using dpid directly in the query of currents can save plenty of time!
    dpid_rows_query = """select distinct substr(lal.alias,INSTR(lal.alias,  '/', -1, 2)+1), id from
    (select max(since) as cd, alias from  cms_trk_dcs_pvss_cond.aliases group by alias) md, cms_trk_dcs_pvss_cond.aliases lal
    join cms_trk_dcs_pvss_cond.dp_name2id on dpe_name=concat(dpname,'.')
    where md.alias=lal.alias and lal.since=cd
    and (lal.alias like """ + options + """)"""
    #dpid_rows_query ="""select dpe_name, alias, id from cms_trk_dcs_pvss_cond.aliases join cms_trk_dcs_pvss_cond.dp_name2id on (dpname || '.' = dpe_name) where alias like """ + options
    if not testwithoutdb:
        cursor.execute(dpid_rows_query)
        dpid_rows = cursor.fetchall()
    else:
        print dpid_rows_query
    rows = {}
    quantity = quantity[0].upper() + quantity[1:].lower()

    ### Define query to get temperatures for Barrel Pixels
    if not testwithoutdb:
        for k in range(len(dpid_rows))[:]:
            moduleName = str(dpid_rows[k][0])
            moduleName = moduleName# [59:]
            dpidname = dpid_rows[k][1]
            query = """select actual_""" + quantity + """, change_date from cms_trk_dcs_pvss_cond.fwcaenchannel         where change_date between TO_TIMESTAMP('""" + starttime + """', 'RRRR-MM-DD HH24:MI:SS.FF') and TO_TIMESTAMP('""" + stoptime + """', 'RRRR-MM-DD HH24:MI:SS.FF') and dpid='""" + str(dpidname) + """' and actual_""" + quantity + """ is not NULL         order by change_date"""

            cursor.execute(query)
            rows[moduleName] = cursor.fetchall()
        connection.close()

    return rows




if __name__ == "__main__":

    db = "biascurrents.db"
    tb = "biascurrents"
    quantity = 'imon'
    powersupply = {1: '14', 2: '23', 3: '23', 4: '14'}
    hvchannel = {1: '1', 2: '2', 3: '1', 4: '2'}
    minibiasscan_v2 = {
        "L1" : ["bpix","bmo","7","lay14","HV1"],
        "L2" : ["bpix","bpi","6","lay23","HV2"],
        "L3" : ["bpix","bpo","3","lay23","HV1"],
        "L4" : ["bpix","bmi","3","lay14","HV2"],
        "R1" : ["fpix","bmi","3","rog3","HV%"],
        "R2" : ["fpix","bpo","1","rog1","HV2"]}


    minibiasscan_v1 = {
        "L1" : ["bpix","bmo","2","lay14","HV1"],
        "L2" : ["bpix","bpi","6","lay23","HV2"],
        "L3" : ["bpix","bpo","3","lay23","HV1"],
        "L4" : ["bpix","bmi","3","lay14","HV2"],
        "R1" : ["fpix","bmi","3","rog3","HV%"],
        "R2" : ["fpix","bpo","1","rog1","HV2"]}

    channel = ''
    layers = ''
    disk = ''
    #analog:    channel = 1
    #digital:   channel = 0

    headers = [quantity, 'change_date' ]

    scanlogs = glob.glob('biasscans/*20180419*')
    scanlogs += glob.glob('biasscans/*201809*')
    scanlogs.extend(glob.glob('biasscans/*full*'))
    scanlogs = glob.glob('biasscans/*')
    print scanlogs
    firstbiasscanlog = 'biasscans/hvscan_20180419-051023.txt'
    for biasscanlog in scanlogs:
        print 'looking at', biasscanlog
        scandate = biasscanlog.split('_')[1].split('-')[0]

        if 'mini' in biasscanlog.lower():
            print scandate
            if int(scandate) < 20180815:
                scantype = 'miniv1'
            else:
                scantype = 'miniv2'
            print scantype
        elif 'full' in biasscanlog or 'hvscan' in biasscanlog:
            scantype = 'full'
        else:
            print biasscanlog, 'what kind of scan was that?'
            sys.exit()
        with open(biasscanlog, 'r') as readbias:
            for line in readbias:
                if 'timestamp' in line:
                    values = line.replace(' -- ', ',').split(',')
                    #d = {val[:val.index(':')].strip(): val[val.index(':') + 1:].strip() for val in values if ':' in val}
                    d = {}
                    for val in values:
                        if ':' in val:
                            d[val[:val.index(':')].strip()] = val[val.index(':') + 1:].strip()

                    if 'start' in line:
                        #values_from_scan = {scankey: int(d[scankey]) if not 'time' in scankey else d[scankey] for scankey in d }
                        values_from_scan = {}
                        for scankey in d:
                            if not 'time' in scankey:
                                values_from_scan[scankey] = int(d[scankey])
                            else:
                                values_from_scan[scankey] = d[scankey]

                        values_from_scan['scantype'] = scantype

                        values_from_scan['starttime'] = values_from_scan['timestamp(UTC)']
                    elif 'stop' in line:
                        values_from_scan['stoptime'] = d['timestamp(UTC)']
                        for layer in range(1, 5):
                        #for layer in [2]:
                            values_from_scan['vset_from_scan'] = values_from_scan['L' + str(layer)]
                            values_from_scan['layer'] = layer
                            values_from_scan['scanfile'] = biasscanlog
                            values_from_scan['scandate'] = scandate
                            layers = int(powersupply[int(layer)])
                            channel = int(hvchannel[int(layer)]) + 1
                            if layer == 1:
                                halfshell = 'BmO'
                                if scantype == 'miniv1':
                                    sectorrog = 2
                                else:
                                    sectorrog = 7
                            if layer == 2:
                                halfshell = 'BpI'
                                sectorrog = 6
                            if layer == 3:
                                halfshell = 'BpO'
                                sectorrog = 3
                            if layer == 4:
                                halfshell = 'BmI'
                                sectorrog = 3
                            currents_voltages = vmon_imon(values_from_scan['starttime'], values_from_scan['stoptime'], layers=layers, disk=disk, halfshell=halfshell, sectorsrogs=sectorrog, channel=channel, quantity=quantity, testwithoutdb=testwithoutdb)
                            for modname in currents_voltages:
                                if scantype == 'miniv1':
                                    if minibiasscan_v1['L' + str(layer)][2] not in modname.lower() and 'S' + minibiasscan_v1['L' + str(layer)][3] not in modname.lower():
                                        print scantype, 'skipping', modname, 'for layer', layer, minibiasscan_v1['L' + str(layer)]
                                if currents_voltages[modname] != []: print 'sector:', modname
                                #for vals in currents_voltages[modname]:
                                if currents_voltages[modname] == []:
                                    starttime = values_from_scan['starttime']
                                    starttimepython = datetime.datetime.strptime(starttime,  '%Y-%m-%d %H:%M:%S')
                                    newstarttimepython = starttimepython - datetime.timedelta(minutes = 2)
                                    newstarttime = newstarttimepython.strftime('%Y-%m-%d %H:%M:%S')
                                    stoptime = values_from_scan['stoptime']
                                    stoptimepython = datetime.datetime.strptime(stoptime,  '%Y-%m-%d %H:%M:%S')
                                    newstoptimepython = stoptimepython - datetime.timedelta(minutes = 2)
                                    newstoptime = newstoptimepython.strftime('%Y-%m-%d %H:%M:%S')
                                    currents_voltages = vmon_imon(newstarttime, newstoptime, layers=layers, disk=disk, halfshell=halfshell, sectorsrogs=sectorrog, channel=channel, quantity=quantity, testwithoutdb=testwithoutdb)
                                else: 
                                    continue
                                if currents_voltages[modname] == []:
                                    print 'sector:', modname, 'layer', layer, biasscanlog, values_from_scan['stoptime'], 'no values found!'
                                    continue

                                for vals in [currents_voltages[modname][-1]]:

                                    values = {}
                                    for key in values_from_scan:
                                        values[key] = values_from_scan[key]
                                    for i in range(len(headers)):
                                        #print vals
                                        #if type(vals[i]) == str:
                                        #    values[headers[i]] =  vals[i].strip('cms_trk_dcs_4:')
                                        #else:
                                        #    values[headers[i]] =  vals[i]
                                        values[headers[i]] = vals[i]
                                    if values[quantity] != None:
                                        print quantity, values[quantity], 'found at', datetime.datetime.isoformat(values['change_date']), "for", modname, 'with vset', values_from_scan['vset_from_scan']
                                    values.update(values_from_scan)
                                    values['modname'] = modname
                                    print values
                                    if not testwithoutdb:
                                        sql.dict_to_db(values, db, tb)








