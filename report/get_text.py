import os
import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as mdates
import ast
from cycler import cycler
import sys
import argparse






if __name__ == "__main__":


    parser = argparse.ArgumentParser(description='Get all tracker plots.')
    parser.add_argument('--no-wget', '-n', dest='donotwget', action='store_true', help='Do not wget the html and textfiles for the plots again.')
    args = parser.parse_args()
    wget = not args.donotwget
    print 'getting dewpoints:', wget
    dewpoints = {}
    #dewpoints['Sniffer dew points (all but PP1 and external)'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d;enddate=last14d;nhours=14;timediv=d;p_title=Sniffer%20dew%20points%20%28all%20but%20PP1%20and%20external%29;height=600;width=1200;legend=on;location=ne;snf=215143;snf=215132;snf=215128;snf=215142;snf=215141;snf=215127;snf=215140;snf=215135;snf=215131;snf=215134;snf=215145;snf=215133;snf=215139;snf=215146;snf=215117;snf=215130;snf=215136;snf=215144;snf=215138;snf=215129;min=-65;max=-15;rmin=;rmax=;avgdg=1;prescale=1;.cgifields=RH;.cgifields=snfcorr;.cgifields=T;.cgifields=DP;.cgifields=legend;.cgifields=columns;.cgifields=MAR;.cgifields=todpp'
    #dewpoints['Sniffer dew points (PP1)'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Sniffer+dew+points+PP1&height=600&width=1200&legend=on&location=ne&snf=215149&snf=215148&snf=215137&snf=215151&min=-65&max=-15&rmin=&rmax=&avgdg=1&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    dewpoints['Sniffer dew points (external)'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Sniffer+dew+points+external&height=600&width=1200&legend=on&location=ne&snf=215150&snf=215147&min=&max=&rmin=&rmax=&avgdg=1&prescale=5&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'


    for sniffers in dewpoints:
        dewpointcommand = "wget '"
        dewpointcommand += dewpoints[sniffers]
        dewpointcommand += "' -O dewpoints.html"

        if wget:
            print dewpointcommand
            out = os.popen(dewpointcommand).readlines()
            print 'got new dewpoints.html'
            print out
        f = open('dewpoints.html', 'r')
        colors = ['blue', 'magenta', 'orange', 'green', 'cyan']
        line = f.readline()
        while line != '' and not line.strip().startswith('<h3>Raw Data:</h3>'):
            line = f.readline()
        txtfiles = line.split('</a>')

        fig = plt.figure()
        ax = plt.subplot(111)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
        colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
        order = range(len(colors))
        ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
        number = 0

        for txtfile in txtfiles:
            if '=' not in txtfile:
                print "skipping this line:"
                print txtfile
                continue
            if 'epoch' in txtfile: continue
            filename = txtfile[txtfile.index('=') + 1: txtfile.index('">') + 1].strip('"')
            plotname = txtfile[txtfile.index('">') + 2:]
            print "filename", filename
            print "plotname", plotname


            wgetcommand = "wget 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/" + filename + "' -O " + plotname + ".txt"
            if wget:
                print wgetcommand
                dowget = os.popen(wgetcommand).readlines()
                print dowget
                print 'got new textfile', plotname

            temps = [l.strip().split() for l in open(plotname + '.txt').readlines()]
            temps = [[t[0] + ' ' + t[1], t[2]] for t in temps]
            color = colors[number % len(colors)]
            dates = [mdates.date2num(datetime.strptime(t[0], '%Y-%m-%d %H:%M:%S')) for t in temps]
            ax.scatter(dates, [t[1] for t in temps], color=color, label=plotname)
            number += 1
            line = f.readline()
            print 'plot number', number


        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax.grid(True)
        plt.title(sniffers)

        # Rotate dates
        fig.autofmt_xdate(rotation=45)
        fig.tight_layout()

        mindate = min(dates)
        maxdate = max(dates)

        # Add degree label

        plt.ylabel('$^\circ$C', fontsize=20)
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)
        plt.xlim(mindate, maxdate)
        plt.savefig(sniffers.replace(' ', '_').replace('(', '_').replace(')', '_') + '_dewpoints.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight')
