#!/usr/bin/env zsh


if [[ -a $1 ]]
    then
        cookiefile=$1
    else
        echo Usage:
        echo ./curlwbmppng.sh cookiefile pngfilename
        cookiefile='cookiefiledemo.txt'
        cookiefile=$1
fi

if [[ -a $2 ]]
    then
        pngfile=$2
    else
        echo Usage:
        echo ./curlwbmppng.sh cookiefile pngfilename
        pngfile='https://cmswbm.cern.ch/cmsdb/data/ConditionBrowser_1502388796906.png' 
        pngfile=$1
fi

if [[ -a $cookiefile ]]
then
    cookie=$(cat $cookiefile)
else
    echo no cookiefile $cookiefile found
    cookie='empty'
fi


echo 'cookiefile:' $cookiefile
echo 'pngfile:' $pngfile
echo ''
echo 'You entered:'
echo ./curlwbmpng.sh $cookiefile $pngfile
echo 'Example use:'
echo ./curlwbmpng.sh cookiefiledemo.txt https://cmswbm.cern.ch/cmsdb/data/ConditionBrowser_1502388796906.png

echo curl -o wbm.png -k \
    -H 'Host: cmswbm.cern.ch'  \
    -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0'  \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'  \
    -H 'Accept-Language: en-US,en;q=0.5' --compressed  \
    -H 'Cookie: '${cookie} \
    -H 'DNT: 1'  \
    -H 'Connection: keep-alive'  \
    -H 'Upgrade-Insecure-Requests: 1'
#https://cmswbm.cern.ch/cmsdb/data/ConditionBrowser_1502387585346.eps