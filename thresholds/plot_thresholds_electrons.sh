#!/usr/bin/env zsh



if [[ -a $1 ]]
    then
        echo will make nice plots on on $1 for layer $2
    else
        echo usage:
        echo ./run_thresholds.sh filename
        echo for example
        echo ./run_thresholds.sh Run1543_thresholdLYR1_20170904_190121.root
fi

if [[ $1 =~ '.*LYR1.*' ]]
then
    echo using layer 1
    layer=1
else
    echo using layer 2-4
    layer=2
fi

#if [[ -a $2 ]]
#then
#    layer=$2
#else
#    layer=1
#    echo no layer specified
#    echo use 1 for layer 1 and 2 for layers 2-4
#fi
#echo will assume layer $layer

export QUOTES='"'
export FILENAME=$1
#$QUOTES$1$QUOTES

echo "Using file "$FILENAME
savename=$QUOTES$FILENAME[0,-6]$QUOTES
echo Saving with name $savename

if [[ $layer == 1 ]]
then
    nice="niceL1.C"
    layername="1"
else
    nice=$QUOTES"niceL2-4.C"$QUOTES
    layername=$QUOTES"2-4"$QUOTES
fi

root -b -l << EOF
.L $nice
nice($savename)
.q
EOF

#main($FILENAME)
