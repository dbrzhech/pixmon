void nice(std::string filename) {
  //std::string filename = "Run1543_thresholdLYR2-4_20170904_190121";
  TFile f((filename+".root").c_str());
  c = (TCanvas*) f.Get("c1");
  TH1D* h = (TH1D*) c->GetPrimitive("LYR2-4");

  //gStyle->SetOptStat(0);
  TCanvas c2("c","c",1000,1000);
  c2.SetLogy(0);
  c2.SetGrid(1);
  c2.SetLeftMargin(0.22);
  h->SetTitle("threshold distribution BPix layers 2-4");
  h->GetYaxis()->SetTitleOffset(1.7);  
  h->GetYaxis()->SetTitle("number of ROCs");  
  h->GetXaxis()->SetTitleSize(0);  
  h->GetXaxis()->SetLabelSize(0);
  h->GetXaxis()->SetTickLength(0);
  h->GetXaxis()->SetTitle("");  
  h->SetLineWidth(2);
  h->SetLineColor(kBlack);
  h->SetMaximum(1.2*h->GetMaximum());
  h->Draw();

  TGaxis *axis = new TGaxis(0,0,100,0,0,5,505,"+L");
  axis->SetTitle("threshold [1000 e^{-}]");
  axis->SetLabelFont(42);
  axis->SetLabelSize(0.06);
  axis->SetLabelOffset(0.015);
  axis->SetTitleFont(42);
  axis->SetTitleSize(0.07);
  axis->SetTitleOffset(1.0);
  axis->Draw();

  TLatex* latex = new TLatex();
  latex->SetNDC();
  latex->SetTextFont(61);
  latex->SetTextAlign(11);
  latex->DrawLatex(0.43, .87, "CMS Pixel");
  latex->SetTextFont(51);
  latex->DrawLatex(0.66, .87, "Preliminary");
  latex->SetTextFont(41);  
  latex->SetTextSize(0.035);  
  latex->DrawLatex(0.72, .21, "27.08.2017");

  c2.SaveAs((filename+"_nice.png").c_str());
  c2.SaveAs((filename+"_nice.pdf").c_str());
}
